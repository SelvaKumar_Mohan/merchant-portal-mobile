package com.paysafe.merchant_portal

import com.google.firebase.FirebaseApp
import io.flutter.app.FlutterApplication

class MerchantPortalApplication : FlutterApplication() {
    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this);
    }
}