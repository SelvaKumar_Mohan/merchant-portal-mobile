import 'package:flutter_test/flutter_test.dart';
import 'package:merchant_portal/ocr_view.dart';

void main() {
  test('Test card number recognition', () {
    String lineText = '1234 5678 9101 1121';
    RegExp pattern = new RegExp(r'({[0-9]{4}}    )');
    RegExpMatch firstMatch = pattern.firstMatch(lineText);
    if (firstMatch != null)
    print(firstMatch.group(0));
    bool isCardNumber = pattern.hasMatch(lineText);
    if (isCardNumber)
      print('CardNumber Detected $lineText');
    else
      print('Not a card number');
  });
}