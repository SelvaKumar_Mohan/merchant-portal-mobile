import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:merchant_portal/find_merchants/merchant_location_data.dart';
import 'package:merchant_portal/find_merchants/merchant_location_view.dart';
import 'package:merchant_portal/find_merchants/repository.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'dart:developer' as developer;

class MapView extends StatefulWidget {
  List<MerchantLocationData> merchantLocationData;

  MapView() {
    merchantLocationData = [
      MerchantLocationData(
          name: 'Kalanjali Arts & Craft',
          id: 'id_more',
          position: LatLng(17.432714, 78.387359),
          description: '5-10-194, Hill Fort Road, Saifabad'),
      MerchantLocationData(
          name: 'PCH Retail Ltd',
          id: 'id_heritage',
          position: LatLng(17.433973, 78.386307),
          description: '102 to 107 Maheswari Chambers'),
      MerchantLocationData(
          name: 'Thirdi Digital',
          id: 'id_more',
          position: LatLng(17.435273, 78.386940),
          description: '8-2-270, Banjara Hills Road'),
      MerchantLocationData(
          name: 'Sail Silks Kalamandir Ltd',
          id: 'id_more',
          position: LatLng(17.434321, 78.385116),
          description: '6-3-790/8, Flat #1 Bathina Apartments'),
      MerchantLocationData(
          name: 'EYantra Industries Ltd',
          id: 'id_more',
          position: LatLng(17.436133, 78.385524),
          description: '6-2-1012 TGV Mansion 3rd floor'),
    ];
  }

  @override
  _MapViewState createState() => _MapViewState();
}

class _MapViewState extends State<MapView> {
  GoogleMapController mapController;
  double merchantVisinityRadiusInMeter = 100;
  List<MerchantLocationData> merchantLocationData = [];
  bool isNearByMerchantsLoading = true;

  final Repository repository = new Repository();
  final Location location = new Location();
  final double _defaultZoomLevel = 17;

  static const LatLng _defaultLocation = LatLng(17.3850, 78.4867);

  LatLng _currentLocation;

  _MapViewState() {
    _currentLocation = _defaultLocation;
    _updateCurrentPosition();
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        _getCameraPosition(position: _currentLocation),
      ),
    );
  }

  CameraPosition _getCameraPosition({LatLng position = _defaultLocation}) {
    return CameraPosition(
      target: position,
      zoom: _defaultZoomLevel,
    );
  }

  Set<Marker> _getMarkers() {
    Set<Marker> markers = Set();
    merchantLocationData.forEach((data) => {
          markers.add(
            Marker(
              markerId: MarkerId(data.id),
              position: data.position,
              icon: BitmapDescriptor.defaultMarker,
              infoWindow:
                  InfoWindow(title: '${data.name}', snippet: '${data.address}'),
            ),
          ),
        });
    developer.log('markers length ${markers.length}', name: 'MapView');
    return markers;
  }

  @override
  Widget build(BuildContext context) {
    developer.log(
        'Updated merchantVisinityRadius $merchantVisinityRadiusInMeter',
        name: 'MapView');
    return Column(
      children: <Widget>[
        Expanded(
          flex: 3,
          child: Stack(
            children: <Widget>[
              GoogleMap(
                onMapCreated: _onMapCreated,
                mapType: MapType.normal,
                markers: _getMarkers(),
                initialCameraPosition:
                    _getCameraPosition(position: _currentLocation),
                myLocationEnabled: true,
                compassEnabled: true,
                circles: Set.from([
                  Circle(
                    circleId: CircleId('current_location'),
                    visible: true,
                    strokeWidth: 0,
                    center: _currentLocation,
                    radius: merchantVisinityRadiusInMeter,
                    fillColor: Color.fromARGB(100, 135, 206, 250),
                  ),
                ]),
              ),
              Positioned(
                top: 50,
                right: 10,
                child: RotatedBox(
                  quarterTurns: 3,
                  child: Container(
                    child: Slider(
                      value: merchantVisinityRadiusInMeter / (100 * 10),
                      divisions: 4,
                      label: '$merchantVisinityRadiusInMeter mt',
                      onChanged: (newValue) {
                        setState(() {
                          merchantVisinityRadiusInMeter = newValue * 10 * 100;
                          developer.log(
                              'merchantVisinityRadius $merchantVisinityRadiusInMeter',
                              name: 'MapView');
                        });
                        repository
                            .getNearByMerchants(
                                _currentLocation, merchantVisinityRadiusInMeter)
                            .then((nearByMerchants) {
                          developer.log('$nearByMerchants',
                              name: 'NearByMerchants');
                          setState(() {
                            merchantLocationData = nearByMerchants;
                          });
                        });
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          flex: 1,
          child: MerchantLocationView(
            merchantLocationData: merchantLocationData,
          ),
        ),
      ],
    );
  }

  void _updateCurrentPosition() async {
    var currentPosition = await location.getLocation();
    developer.log('CurrentPostion ${currentPosition}', name: 'MapView');
    _currentLocation = new LatLng(
      currentPosition.latitude,
      currentPosition.longitude,
    );
    repository
        .getNearByMerchants(_currentLocation, merchantVisinityRadiusInMeter)
        .then((nearByMerchants) {
      developer.log('$nearByMerchants', name: 'NearByMerchant');
      setState(() {
        merchantLocationData = nearByMerchants;
        if (mapController != null)
          mapController.animateCamera(
            CameraUpdate.newCameraPosition(
              _getCameraPosition(position: _currentLocation),
            ),
          );
      });
    });
  }
}
