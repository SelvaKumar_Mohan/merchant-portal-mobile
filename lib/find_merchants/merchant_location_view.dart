import 'package:flutter/material.dart';
import 'package:merchant_portal/find_merchants/merchant_location_data.dart';

class MerchantLocationView extends StatefulWidget {
  List<MerchantLocationData> merchantLocationData;

  MerchantLocationView(
      {Key key, @required List<MerchantLocationData> merchantLocationData})
      : super(key: key) {
    this.merchantLocationData = merchantLocationData;
  }

  @override
  _MerchantLocationViewState createState() => _MerchantLocationViewState();
}

class _MerchantLocationViewState extends State<MerchantLocationView> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.only(top: 10.0),
        child: SizedBox(
          child: ListView.builder(
            itemBuilder: (context, index) {
              return Container(
                width: 300,
                child: Card(
                  child: Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      Container(
                        child: Image.network(
                          widget.merchantLocationData[index].iconUri,
                          fit: BoxFit.cover,
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              widget.merchantLocationData[index].name,
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              widget.merchantLocationData[index].address,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 15),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              );
            },
            scrollDirection: Axis.horizontal,
            itemCount: widget.merchantLocationData.length,
          ),
        ),
      ),
    );
  }
}
