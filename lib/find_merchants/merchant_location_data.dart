import 'package:google_maps_flutter/google_maps_flutter.dart';

class MerchantLocationData {
  MerchantLocationData({
    this.name,
    this.id,
    this.position,
    this.description,
    this.rating,
    this.iconUri,
    this.address,
  });

  factory MerchantLocationData.fromJson(Map<String, dynamic> json) {
    var geometry = json['geometry'];
    var location = geometry['location'];
    LatLng position = LatLng(location['lat'], location['lng']);
    String iconUri = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=300&photoreference=CmRaAAAACvUNYzpUE1goSzJlcPGeo2JY6cS6mKkWzmyO9NFA1k261Zq0tE0PZGbPB0GN6zJoDbkPBS2mntf-O2tPGteq-DFtkwkGCvzjeHe7fdKMjpHVe7CF9Q8ejcSgVniSbtTCEhDqddiz9bSC-IDInosuE6CgGhRDqNwlLeg-JW5oqOO0Bj-gWXUaaw&key=AIzaSyC4XdI8-zeCPkr5z5BPGpFUNzhT4h3A-XA';
    var photos = json['photos'];
    if (photos != null) {
      var photo = photos[0];
      iconUri = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=300&photoreference=${photo['photo_reference']}&key=AIzaSyC4XdI8-zeCPkr5z5BPGpFUNzhT4h3A-XA';
    }
    String name = json['name'];
    String id = json['place_id'];
    int rating = json['user_ratings_total'];
    String address = json['vicinity'];
    String description = json['types'][0];

    return MerchantLocationData(
      id: id,
      name: name,
      address: address,
      description: description,
      position: position,
      iconUri: iconUri,
      rating: rating,
    );
  }

  String name;
  String id;
  String address;
  String iconUri;
  int rating;
  LatLng position;
  String description;

  @override
  String toString() {
    return '{id: $id, name: $name, address: $address, iconUri: $iconUri, rating: $rating, position: $position, description: $description}';
  }


}