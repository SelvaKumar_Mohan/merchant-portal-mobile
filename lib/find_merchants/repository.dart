import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:merchant_portal/find_merchants/merchant_location_data.dart';

class Repository {

  Future<List<MerchantLocationData>> getNearByMerchants(LatLng myLocation, double boundaryInMeter) async{
      List<MerchantLocationData> nearByMerchantsLocationData = [];

      String url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='
          '${myLocation.latitude},${myLocation.longitude}&radius=${boundaryInMeter}&types=store&key=AIzaSyC4XdI8-zeCPkr5z5BPGpFUNzhT4h3A-XA';
      http.Response nearByMerchantsResponse = await http.get(url);
      if (nearByMerchantsResponse.statusCode == 200) {
        var nearByMerchantsResponseBody = json.decode(nearByMerchantsResponse.body);
        var nearByMerchantsResult = nearByMerchantsResponseBody['results'];
        nearByMerchantsResult.forEach((merchant) {
          MerchantLocationData merchantLocationData = MerchantLocationData.fromJson(merchant);
          nearByMerchantsLocationData.add(merchantLocationData);
        });
      }
      return nearByMerchantsLocationData;
  }
}