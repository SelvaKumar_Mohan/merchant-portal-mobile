import 'package:flutter/material.dart';
import 'package:merchant_portal/common_widgets.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final usernameFocusNode = FocusNode();
  final passwordFocusNode = FocusNode();

  String username = '';
  String password = '';

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    bool _autoValidate = false;

    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(top: 30.0, bottom: 30.0),
            color: Colors.blue,
            width: double.infinity,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 20.0, bottom: 30.0),
                  child: Text(
                    "Welcome to Paysafe Business Portal",
                    textDirection: TextDirection.ltr,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 35,
                    ),
                  ),
                ),
                Text(
                  "Login to access all the ways your customers pay",
                  textDirection: TextDirection.ltr,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w300),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.only(
                top: 50.0, bottom: 50.0, left: 10.0, right: 10.0),
            child: Form(
              key: formKey,
              autovalidate: _autoValidate,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  getPrimaryTextField(
                    'Username',
                    value: username,
                    focusNode: usernameFocusNode,
                    margin: const EdgeInsets.only(
                      bottom: 20.0,
                    ),
                    fieldValidator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter username';
                      }
                      return null;
                    },
                    onFieldSubmitted: (value) {
                      username = value;
                      FocusScope.of(context).requestFocus(passwordFocusNode);
                    },
                    textInputAction: TextInputAction.next,
                  ),
                  getPrimaryTextField(
                    'Password',
                    value: password,
                    focusNode: passwordFocusNode,
                    margin: const EdgeInsets.only(bottom: 20.0),
                    isPassword: true,
                    fieldValidator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter password';
                      }
                      return null;
                    },
                    onFieldSubmitted: (value) {
                      password = value;
                    },
                    textInputAction: TextInputAction.done,
                  ),
                  Container(
                    child: FlatButton(
                      color: Colors.blue,
                      onPressed: () {
                        Navigator.pushNamedAndRemoveUntil(context, '/dashboard', (route) => false);
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 13.0, bottom: 13.0),
                        child: Text(
                          "Login",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.w300),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 30.0, bottom: 30.0),
                    child: Text(
                      'Forgot your passoword ?',
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
