import 'package:flutter/material.dart';

class NotAvailableView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Text('Not yet implemented.', style: TextStyle(
        color: Colors.black,
        fontSize: 20
      ),),
    );
  }
}
