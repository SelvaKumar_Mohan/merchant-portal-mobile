import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:permission_handler/permission_handler.dart';

class CardPaymentView extends StatefulWidget {
  @override
  _CardPaymentViewState createState() => _CardPaymentViewState();
}

class _CardPaymentViewState extends State<CardPaymentView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 5.0,),
              alignment: Alignment.centerLeft,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Add New Card',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 18,
                    ),
                  ),
                  IconButton(
                    onPressed: () async{
                      await PermissionHandler().requestPermissions(
                        [
                          PermissionGroup.camera,
                          PermissionGroup.microphone,
                          PermissionGroup.storage
                        ],
                      );
                     Navigator.pushNamed(context, '/scan_card');
                    },
                    icon: Icon(Icons.photo_camera),
                  )
                ],
              ),
            ),
            Divider(
              thickness: 2.0,
            ),
            Container(
              margin: EdgeInsets.only(left: 15.0, right: 15.0),
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(
                      top: 15.0,
                    ),
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: 'XXXX XXXX XXXX XXXX',
                        labelText: 'Card Number',
                        contentPadding: EdgeInsets.only(top: 10),
                        labelStyle: TextStyle(
                          fontSize: 16,
                        ),
                        alignLabelWithHint: false,
                        hasFloatingPlaceholder: true,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: 15.0,
                    ),
                    child: TextField(
                      decoration: InputDecoration(
                        labelText: 'Cardholder Name',
                        contentPadding: EdgeInsets.only(top: 10),
                        labelStyle: TextStyle(
                          fontSize: 16,
                        ),
                        alignLabelWithHint: false,
                      ),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 15.0,
                            right: 5.0,
                          ),
                          child: TextField(
                            decoration: InputDecoration(
                              labelText: 'Valid Thru',
                              hintText: 'MM',
                              contentPadding: EdgeInsets.only(top: 10),
                              labelStyle: TextStyle(
                                fontSize: 16,
                              ),
                              alignLabelWithHint: false,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 15.0,
                            right: 5.0,
                          ),
                          child: TextField(
                            decoration: InputDecoration(hintText: 'YYYY'),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 15.0,
                          ),
                          child: TextField(
                            decoration: InputDecoration(
                              labelText: 'CVV',
                              contentPadding: EdgeInsets.only(top: 10),
                              labelStyle: TextStyle(
                                fontSize: 16,
                              ),
                              alignLabelWithHint: false,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Container(
              width: 200,
              margin: EdgeInsets.only(
                top: 20.0,
              ),
              decoration: BoxDecoration(
                  color: Colors.blue,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(
                    25.0,
                  )),
              child: FlatButton(
                onPressed: () {},
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    'Save',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
