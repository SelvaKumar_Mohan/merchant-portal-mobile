import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:merchant_portal/dashboard.dart';
import 'package:merchant_portal/find_merchants/map_view.dart';
import 'package:merchant_portal/login_view.dart';
import 'package:merchant_portal/merchant_info_view.dart';
import 'package:merchant_portal/ocr_view.dart';
import 'package:merchant_portal/video_pages/call.dart';

void main() => runApp(MerchantPortal());

class MerchantPortal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.blue));
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: DashBoardView(),
      routes: {
        '/login': (context) => LoginView(),
        '/dashboard': (context) => DashBoardView(),
        '/merchant_info': (context) => MerchantInfoView(),
        '/nearyby_merchants': (context) => MapView(),
        '/scan_card': (context) => OCRView(),
        '/video_call': (context) => CallPage(
              channelName: 'customer_support',
            ),
      },
    );
  }
}
