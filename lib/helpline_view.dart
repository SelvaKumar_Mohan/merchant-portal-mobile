import 'package:flutter/material.dart';

class HelplineView extends StatefulWidget {
  @override
  _HelplineViewState createState() => _HelplineViewState();
}

class _HelplineViewState extends State<HelplineView> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              MessageBody('Hello there!', true),
              MessageBody('Yes', false),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 10.0, bottom: 10.0, top: 10.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: TextFormField(
                  decoration: InputDecoration(
                      hintText: 'Enter your message',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          borderSide: BorderSide(color: Colors.blue))),
                ),
              ),
              Container(
                child: RawMaterialButton(
                  child: Icon(
                    Icons.send,
                    color: Colors.white,
                  ),
                  onPressed: () {},
                  shape: CircleBorder(),
                  fillColor: Colors.blue,
                  padding: EdgeInsets.all(15.0),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}

class MessageBody extends StatelessWidget {
  bool isClientMessage;

  String message;

  MessageBody(this.message, this.isClientMessage);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: isClientMessage ? Alignment.centerLeft : Alignment.centerRight,
      child: Container(
        margin: EdgeInsets.all(10.0),
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          color: isClientMessage ? Colors.white : Colors.blue,
          border: Border.all(
            color: isClientMessage ? Colors.grey : Colors.blue,
          ),
          borderRadius: BorderRadius.all(Radius.circular(15.0)),
        ),
        child: Text(
          message,
          style: TextStyle(
            fontSize: 16,
            color: isClientMessage ? Colors.black : Colors.white,
          ),
        ),
      ),
    );
  }
}
