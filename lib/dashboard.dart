import 'package:flutter/material.dart';
import 'package:merchant_portal/card_payment/card_payment_view.dart';
import 'package:merchant_portal/find_merchants/map_view.dart';
import 'package:merchant_portal/helpline_view.dart';
import 'package:merchant_portal/home_view.dart';
import 'package:permission_handler/permission_handler.dart';

import 'not_available_view.dart';

class DashBoardView extends StatefulWidget {
  @override
  _DashBoardViewState createState() => _DashBoardViewState();
}

class _DashBoardViewState extends State<DashBoardView> {
  Widget viewBody = HomeView();
  String title = 'My Accounts';

  commonOnTap() {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        actions: <Widget>[
          (title == 'Helpline')
              ? IconButton(
                  icon: Icon(Icons.video_call),
                  onPressed: () async {
                    await PermissionHandler().requestPermissions(
                      [PermissionGroup.camera, PermissionGroup.microphone],
                    );
                    Navigator.pushNamed(context, '/video_call');
                  })
              : Container()
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.only(top: 30.0),
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.home),
              title: Text('Home'),
              onTap: () {
                commonOnTap();
                setState(() {
                  viewBody = HomeView();
                  title = 'My Accounts';
                });
              },
            ),
            ListTile(
              leading: Icon(Icons.cached),
              title: Text('Transfers'),
              onTap: () {
                commonOnTap();
                setState(() {
                  title = 'Transfers';
                  viewBody = NotAvailableView();
                });
              },
            ),
            ListTile(
              leading: Icon(Icons.monetization_on),
              title: Text('Balances'),
              onTap: () {
                commonOnTap();
                setState(() {
                  title = 'Balances';
                  viewBody = NotAvailableView();
                });
              },
            ),
            ListTile(
              leading: Icon(Icons.insert_drive_file),
              title: Text('All Transactions'),
              onTap: () {
                commonOnTap();
                setState(() {
                  title = 'All Transactions';
                  viewBody = NotAvailableView();
                });
              },
            ),
            ListTile(
              leading: Icon(Icons.account_balance),
              title: Text('Banking'),
              onTap: () {
                commonOnTap();
                setState(() {
                  title = 'Banking';
                  viewBody = NotAvailableView();
                });
              },
            ),
            ListTile(
              leading: Icon(Icons.perm_identity),
              title: Text('Users'),
              onTap: () {
                commonOnTap();
                setState(() {
                  title = 'Users';
                  viewBody = NotAvailableView();
                });
              },
            ),
            ListTile(
              leading: Icon(Icons.info_outline),
              title: Text('Nearby Merchants'),
              onTap: () {
                commonOnTap();
                setState(() {
                  viewBody = MapView();
                  title = 'Nearby Merchants';
                });
              },
            ),
            ListTile(
              leading: Icon(Icons.payment),
              title: Text('Wallet'),
              onTap: () {
                commonOnTap();
                setState(() {
                  viewBody = CardPaymentView();
                  title = 'Wallet';
                });
              },
            ),
            ListTile(
              leading: Icon(Icons.call),
              title: Text('Helpline'),
              onTap: () {
                commonOnTap();
                setState(() {
                  viewBody = HelplineView();
                  title = 'Helpline';
                });
              },
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Settings'),
              onTap: () {
                commonOnTap();
                setState(() {
                  title = 'Settings';
                  viewBody = NotAvailableView();
                });
              },
            ),
          ],
        ),
      ),
      body: viewBody,
    );
  }
}
