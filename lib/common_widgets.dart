import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget getPrimaryTextField(
  String label, {
  bool isPassword = false,
  EdgeInsets margin = const EdgeInsets.all(10.0),
  Function fieldValidator,
  Function onFieldSubmitted,
  Function onSaved,
  TextInputAction textInputAction,
  FocusNode focusNode,
  String value,

}) {
  if (fieldValidator == null) {
    fieldValidator = (value) {
      return null;
    };
  }
  return Container(
    margin: margin,
    child: TextFormField(
      initialValue: value,
      textInputAction: textInputAction,
      focusNode: focusNode,
      validator: fieldValidator,
      onFieldSubmitted: onFieldSubmitted,
      obscureText: isPassword,
      textDirection: TextDirection.ltr,
      decoration:
          InputDecoration(border: OutlineInputBorder(), labelText: label),
    ),
  );
}
