import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MerchantInfoView extends StatefulWidget {
  var merchantInfo = [
    MerchantInfoFieldModel(
      fieldName: 'Customer ID',
      fieldValue: '123456',
      canEdit: false,
    ),
    MerchantInfoFieldModel(
      fieldName: 'Company Name',
      fieldValue: 'Bet365 Europe',
      canEdit: false,
    ),
    MerchantInfoFieldModel(
      fieldName: 'Company URL',
      fieldValue: 'bet365.eu.com',
      canEdit: true,
    ),
    MerchantInfoFieldModel(
      fieldName: 'Company Representative',
      fieldValue: 'Bruce Wayne',
      canEdit: true,
    ),
    MerchantInfoFieldModel(
      fieldName: 'Contact Name',
      fieldValue: 'Bruce Wayne',
      canEdit: true,
    ),
    MerchantInfoFieldModel(
      fieldName: 'Administrators',
      fieldValue: 'Steve Smith (steve@bet365.com)',
      canEdit: true,
    ),
    MerchantInfoFieldModel(
      fieldName: 'Processing email',
      fieldValue: 'processing@bet365.com',
      canEdit: true,
    ),
    MerchantInfoFieldModel(
      fieldName: 'Technical Email',
      fieldValue: 'tech@bet365.com',
      canEdit: false,
    ),
  ];

  @override
  _MerchantInfoViewState createState() => _MerchantInfoViewState();
}

class _MerchantInfoViewState extends State<MerchantInfoView> {
  @override
  Widget build(BuildContext context) {
    var merchantInfoFields = <Widget>[];

    widget.merchantInfo.forEach((f) {
      merchantInfoFields.add(MerchantInfoFieldView(
        merchantInfoFieldModel: f,
      ));
    });

    print('Merchant data size ${merchantInfoFields.length}');

    return Container(
      child: ListView(
        children: merchantInfoFields,
      ),
    );
  }
}

class MerchantInfoFieldView extends StatefulWidget {
  MerchantInfoFieldModel merchantInfoFieldModel;

  MerchantInfoFieldView({this.merchantInfoFieldModel});

  @override
  _MerchantInfoFieldViewState createState() => _MerchantInfoFieldViewState();
}

class _MerchantInfoFieldViewState extends State<MerchantInfoFieldView> {
  bool showEditInput = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                widget.merchantInfoFieldModel.fieldName,
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 18,
                    fontWeight: FontWeight.w500),
              ),
              widget.merchantInfoFieldModel.canEdit
                  ? InkWell(
                      child: Icon(Icons.edit),
                      onTap: () {
                        setState(() {
                          this.showEditInput = true;
                        });
                      },
                    )
                  : Container()
            ],
          ),
          Container(
            width: double.infinity,
            child: showEditInput
                ? TextFormField(
                    initialValue: widget.merchantInfoFieldModel.fieldValue,
                    onFieldSubmitted: (value) {
                      widget.merchantInfoFieldModel.fieldValue = value;
                      setState(() {
                        showEditInput = false;
                      });
                    },
                  )
                : Text(
                    widget.merchantInfoFieldModel.fieldValue,
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 16,
                        fontWeight: FontWeight.w300),
                  ),
          ),
        ],
      ),
    );
  }
}

class MerchantInfoFieldModel {
  String fieldName;
  String fieldValue;
  bool canEdit;

  MerchantInfoFieldModel({
    this.fieldName,
    this.fieldValue,
    this.canEdit,
  });
}
