import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeView extends StatefulWidget {

  List<AccountInfoModel> accounts;

  HomeView() {
    accounts = generateDummyAccounts();
  }

  List<AccountInfoModel> generateDummyAccounts() {
    List<WalletInfoModel> wallets = [
      WalletInfoModel(currency: 'eur', id: '10607815', balance: 19766.02, reserves: 0.00, isPrimary: true),
      WalletInfoModel(currency: 'bgn', id: '63718321', balance: 00.02, reserves: 0.00, isPrimary: false),
      WalletInfoModel(currency: 'cad', id: '45719246', balance: 1.41, reserves: 0.00, isPrimary: false),
      WalletInfoModel(currency: 'inr', id: '81723926', balance: 645.02, reserves: 0.00, isPrimary: true),
    ];
    List<AccountInfoModel> account = [
      AccountInfoModel(name: 'Bet365 Europe', id: '82313575', wallets: wallets),
      AccountInfoModel(name: 'Bet365 Casino', id: '96816520', wallets: wallets),
      AccountInfoModel(name: 'Bet365 Games', id: '32713132', wallets: wallets),
      AccountInfoModel(name: 'Bet365 ROW', id: '12713530', wallets: wallets),
    ];

    return account;
  }

  @override
  _HomeViewState createState() => _HomeViewState(accounts);
}

class _HomeViewState extends State<HomeView> {

  List<AccountInfoItemView> accountInfoItemViewList;

  _HomeViewState(List<AccountInfoModel> accounts) {
    accountInfoItemViewList = accounts.map((account) {
      return AccountInfoItemView(accountInfo: account, isExpanded: false);
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: ExpansionPanelList(
          expansionCallback: (int index, bool isExpanded) {
            setState(() {
              accountInfoItemViewList[index].isExpanded = !isExpanded;
            });
          },
          children: accountInfoItemViewList.map((account) {
            return ExpansionPanel(
              isExpanded: account.isExpanded,
              headerBuilder: (context, isExpanded) {
                return ListTile(
                  onTap: () {
                    setState(() {
                      account.isExpanded = !isExpanded;
                    });
                  },
                  title: Text(account.accountInfo.name,),
                  subtitle: Text(account.accountInfo.id),
                );
              },
              body: Container(
                child: AccountSummaryView(wallet: account.accountInfo.wallets),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}

class AccountSummaryView extends StatefulWidget {

  AccountSummaryView({this.wallet});

  List<WalletInfoModel> wallet;

  @override
  _AccountSummaryViewState createState() => _AccountSummaryViewState(wallet: wallet);
}

class _AccountSummaryViewState extends State<AccountSummaryView> {

  _AccountSummaryViewState({this.wallet});

  List<WalletInfoModel> wallet;

  @override
  Widget build(BuildContext context) {

    return Container(
      padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0,),
      child: Column(
        children: wallet.map((w) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('${w.getTotalBalance()}', style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                    ),),
                    Text('ID: ${w.id}', style: TextStyle(
                      color: Colors.grey,
                    ),)
                  ],
                )
              ),
              Container(
                child: Text('${w.balance}', style: TextStyle(
                  fontWeight: FontWeight.w700,
                ),),
              ),
              Container(
                child: FlatButton(
                  onPressed: () {},
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text('View'),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.blue)
                  ),

                ),
              ),
            ],
          );
        }).toList(),
      ),
    );
  }
}


class AccountInfoItemView {
  AccountInfoModel accountInfo;
  bool isExpanded;

  AccountInfoItemView({this.accountInfo, this.isExpanded});
}

class AccountInfoModel {
  String name;
  String id;
  List<WalletInfoModel> wallets;

  AccountInfoModel({this.name, this.id, this.wallets});

}

class WalletInfoModel {
  String currency;
  String id;
  double balance;
  double reserves;
  bool isPrimary;

  WalletInfoModel({this.currency, this.id, this.balance, this.reserves, this.isPrimary});

  String getTotalBalance() {
    return '${this.currency.toUpperCase()} ${this.balance}';
  }

}
