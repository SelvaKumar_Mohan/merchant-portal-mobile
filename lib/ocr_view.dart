import 'dart:io';

import 'package:camera/camera.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

class OCRView extends StatefulWidget {
  @override
  _OCRViewState createState() => _OCRViewState();
}

class _OCRViewState extends State<OCRView> {
  TextRecognizer textRecognizer;
  CameraDescription camera;
  CameraController cameraController;
  Future<void> cameraInitializeFuture;
  List<String> info = [];
  bool showProgressBar = true;

  @override
  void initState() {
    availableCameras().then((cameras) {
      if (cameras != null) {
        camera = cameras.first;
        textRecognizer = FirebaseVision.instance.textRecognizer();
        cameras.forEach((cameraDescription) {
          print(
              'camera : ${cameraDescription.name}, ${cameraDescription.lensDirection}');
        });
        cameraController = CameraController(camera, ResolutionPreset.high);
        cameraController.initialize().then((onValue) => {
              this.setState(() {
                showProgressBar = false;
              })
            });
      } else {
        print('No cameras are available');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Scan Card'),
      ),
      body: Container(
          child: (!showProgressBar)
              ? Stack(
                  children: <Widget>[
                    CameraPreview(cameraController),
                    Container(
                      child: ListView.builder(
                        itemBuilder: (context, position) {
                          return Text(
                            info[position],
                            style: TextStyle(color: Colors.red),
                          );
                        },
                        itemCount: info.length,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        margin: EdgeInsets.only(
                          bottom: 25.0,
                        ),
                        padding: EdgeInsets.all(
                          10.0,
                        ),
                        decoration: BoxDecoration(
                            color: Colors.blue, shape: BoxShape.circle),
                        child: FlatButton(
                          onPressed: () async {
                            String path = join(
                                (await getTemporaryDirectory()).path,
                                '${DateTime.now()}.png');
                            await cameraController.takePicture(path);
                            FirebaseVisionImage visionImage =
                                FirebaseVisionImage.fromFilePath(path);
                            VisionText visionText =
                                await textRecognizer.processImage(visionImage);
                            if (visionText != null) {
                              visionText.blocks.forEach((block) {
                                block.lines.forEach((line) {
                                  String lineText = line.text;
                                  checkForCardNumber(lineText);
                                });
                              });
                            } else {
                              print('No text detected from image');
                            }
                          },
                          child: Icon(
                            Icons.photo_camera,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              : Center(
                  child: CircularProgressIndicator(),
                )),
    );
  }

  void checkForCardNumber(String lineText) {
    info.add(lineText);
    print('LineText $lineText');
    lineText = lineText.replaceAll(' ', '');
    RegExp pattern = new RegExp(r'([0-9]{16})');
    RegExpMatch firstMatch = pattern.firstMatch(lineText);
    if (firstMatch != null) {
      print('Card Number Detected: ${firstMatch.group(0)}');
      info.add('Card Detected ${firstMatch.group(0)}');
    }
    setState(() {

    });
  }
}
